/*******************************************************
 * Variable Declarations
 ******************************************************/
var isLogEnabled = true;
var isDiagnosticEnabled = false;
var TC_LIBRARY_DB011 = false;
var TC_LIBRARY_DB012 = false;
var TC_LIBRARY_DB013 = false;
var TC_LIBRARY_DB032 = false;
var TC_LIBRARY_DB041 = false;
var TC_LIBRARY_DB051 = false;


/******************************************************
 * Database Library
 *
 * Functions:
 *      SP.Database.createStore(storeName, storeFields)
 *      SP.Database.clearStore(storeName)
 *      SP.Database.deleteRecord(storeName, record)
 *      SP.Database.addRecord(storeName, dataProperties)
 *      SP.Database.sortRecords(storeName, databaseField, sortType)
 *      SP.Database.filterRecords(storeName, filterField, filterValue)
 *      SP.Database.findRecord(storeName, fieldName, fieldValue)
 *      SP.Database.getRecordValue(storeName, fieldName, fieldValue, getFieldValue)
 *      SP.Database.getFirstRecord(storeName)
 *      SP.Database.getLastRecord(storeName)
 *      SP.Database.getRecordCount(storeName)
 *      SP.Database.updateRecord(storeName, record, fieldName, fieldValue)
 *
 ******************************************************/
var SP = SP || {};

SP.Database = {
    /******************************************************
     * Create Database. (using Html5 localstorage)
     * @param {String} storeName The name of databse in String format
     * @param {Array} storeFields An array of database fields
     ******************************************************/
    createStore: function(storeName, storeFields){
        /** 
         * Handle if store name is not a string type
         */
        if ((typeof(storeName) != 'string')) {
            if (!isDiagnosticEnabled) {
                alert("Error. Create Database - Parameter missing.");
            }
        }
        /** 
         * Handle if store name is null
         */
        else 
            if (storeFields == null) {
                if (!isDiagnosticEnabled) {
                    alert("Error. Fields Null.");
                }
            }
            /** 
         * Handle if store name is a string type
         */
            else 
                if ((typeof(storeFields) == 'string')) {
                    if (!isDiagnosticEnabled) {
                        alert("Error. Fields is a String.");
                    }
                }
                else {
                    /** 
             * Registers the store model
             */
                 /*  Ext.regModel(storeName, {
                        fields: ['id', 'name'],
                        proxy: {
                            type: 'localstorage',
                            id: storeName
                        }
                    });
*/
                  
					Ext.define(storeName, {
					    extend: 'Ext.data.Model',
					    config: {
					        fields: [
					            { name: 'id',     type: 'string' },
					            { name: 'name',   type: 'string' }
					        ],
					         proxy: {
		                            type: 'localstorage',
		                            id: storeName
		                        }
					    },
					});
					


             /* Store automatically picks up the LocalStorageProxy
             * defined on the Search model
             */
                    var newDatabase = storeName;
                    
                    if (isLogEnabled) {
                        console.log("LOG: Database Store Name: " + storeName);
                    }
                    /** 
             * Create the new store
             */
                    window[newDatabase] = new Ext.data.Store({
                        model: storeName
                    // autoLoad: true
                    });
                    /** 
             * Load Database
             */
                    try {
                        window[newDatabase].load();
                        if (isLogEnabled) {
                            console.log("LOG: Database " + storeName + " is successfuly loaded.");
                        }
                    } 
                    catch (err) {
                        if (isLogEnabled) {
                            console.log("LOG: Error. Database not loaded.");
                            console.log(err.description);
                        }
                    }
                }
    }, /* end of createDatabase */
    /******************************************************
     * Clear all the data of the database
     * @param {Html5 localstorage} storeName The name of Html5 database object
     ******************************************************/
    clearStore: function(storeName){
        try {
           // for (var i = storeName.getCount(); i > 0; i--) {
           //     storeName.remove(storeName.last());
           // }
            /** 
             * clear all data from store
             */
           // storeName.getProxy().clear();
            storeName.removeAll();
            storeName.sync();
            if (isLogEnabled) {
                console.log("LOG: Database is cleared.");
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. Database is not cleared.");
                console.log(err.description);
            }
        }
    }, /* end of clearDatabase */
    /******************************************************
     * Delete particular record
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @param {Html5 localstorage} record The record object to be deleted
     *****************************************************/
    deleteRecord: function(storeName, record){
        try {
            /** 
             * Remove the specified data from store
             */
            storeName.remove(record);
            storeName.sync();
            if (isLogEnabled) {
                console.log("LOG: Record deleted");
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. Record not deleted.");
                console.log(err.description);
            }
        }
    }, /* end of deleteRecord */
    /******************************************************
     * Add a new record in the database
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @param {Array} dataProperties The properties of the record to be added
     *****************************************************/
    addRecord: function(storeName, dataProperties){
        try {
            /** 
             * add data to store
             */
            storeName.add(dataProperties);
            storeName.sync();
            if (isLogEnabled) {
                console.log("LOG: Record is added to the database.");
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. Record not added to the database.");
                console.log(err.description);
            }
        }
    }, /* end of addRecord */
    /******************************************************
     * Sorts the Database
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @param {String} databaseField The field to be used for sorting the database
     * @param {String} sortType Type of sort. (Ex. 'ASC' or 'DESC')
     *****************************************************/
    sortRecords: function(storeName, databaseField, sortType){
        if ((typeof(databaseField) == 'string') && (typeof(sortType) == 'string')) {
            try {
                /** 
                 * Sorts the store
                 */
                storeName.sort(databaseField, sortType);
                if (isLogEnabled) {
                    console.log("LOG: Database sorted.");
                }
            } 
            catch (err) {
                if (isLogEnabled) {
                    console.log("LOG: Error. Database not sorted.");
                    console.log(err.description);
                }
            }
        }
        else {
            if (!isDiagnosticEnabled) {
                alert("Error. Invalid Parameter. ");
            }
        }
    }, /* end of sortRecords */
    /******************************************************
     * Filters the Database
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @param {String} filterField The field to be used for filtering the database
     * @param {String} filterValue Value of filter.
     *****************************************************/
    filterRecords: function(storeName, filterField, filterValue){
        if ((typeof(filterField) == 'string') && (typeof(filterValue) == 'string')) {
            try {
                /** 
                 * Filters the store
                 */
                storeName.filter(filterField, filterValue);
                if (isLogEnabled) {
                    console.log("LOG: Database filtered.");
                }
            } 
            catch (err) {
                if (isLogEnabled) {
                    console.log("LOG: Error. Database not filtered.");
                    console.log(err.description);
                }
            }
        }
        else {
            if (!isDiagnosticEnabled) {
                alert("Error. Invalid Parameter. ");
            }
        }
    }, /* end of filterRecords */
    /******************************************************
     * Find the particular record by fieldname and fieldvalue
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @param {String} fieldName Name of the field
     * @param {String} fieldValue Value of of the field.
     * @return {Record Object} record
     *****************************************************/
    findRecord: function(storeName, fieldName, fieldValue){
        var record = "";
        if ((typeof(fieldName) == 'string') && (typeof(fieldValue) == 'string')) {
            try {
                /** 
                 * Find specified record from store
                 */
                record = storeName.findRecord(fieldName, fieldValue);
                if (isLogEnabled) {
                    console.log("LOG: Find record successfully.");
                }
            } 
            catch (err) {
                record = "";
                if (isLogEnabled) {
                    console.log("LOG: Error. Record not found.");
                    console.log(err.description);
                }
            }
            return record;
        }
        else {
            if (!isDiagnosticEnabled) {
                alert("Error. Invalid Parameter. ");
            }
            record = "";
            return record;
        }
    }, /* end of findRecord */
    /******************************************************
     * Get value from specified field/record
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @param {String} fieldName Name of the field
     * @param {String} fieldValue Value of of the field.
     * @param {String} getfieldValue Field where the value is returned.
     * @return {String}  value The Record Value
     *****************************************************/
    getRecordValue: function(storeName, fieldName, fieldValue, getFieldValue){
        var value = "";
        try {
            value = storeName.getAt(storeName.find(fieldName, fieldValue)).get(getFieldValue);
            if (isLogEnabled) {
                console.log("LOG: Find record value successfully.");
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. Record value not found.");
                console.log(err.description);
            }
        }
        return value;
    }, /* end of getRecordValue */
    /******************************************************
     * Get the first record in the database
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @return {Record type} record
     *****************************************************/
    getFirstRecord: function(storeName){
        var record = "";
        try {
            /** 
             * Get the first record in store
             */
            record = storeName.first();
            if (isLogEnabled) {
                console.log("LOG: Find first record successfully.");
            }
        } 
        catch (err) {
            if (!isDiagnosticEnabled) {
                alert("Error. First Record not found.");
            }
            if (isLogEnabled) {
                console.log("LOG: Error. First Record not found.");
                console.log(err.description);
            }
        }
        return record;
    }, /* end of getFirstRecord */
    /******************************************************
     * Get the last record in the database
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @return {Record type} record
     *****************************************************/
    getLastRecord: function(storeName){
        var record = "";
        try {
            /** 
             * Get the last record from store
             */
            record = storeName.last();
            if (isLogEnabled) {
                console.log("LOG: Find last record successfully.");
            }
        } 
        catch (err) {
            if (!isDiagnosticEnabled) {
                alert("Error. Last Record not found.");
            }
            if (isLogEnabled) {
                console.log("LOG: Error. Last Record not found.");
                console.log(err.description);
            }
        }
        return record;
    }, /* end of getLastRecord */
    /******************************************************
     * Get the total number of records in the database
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @return {String} value Total number of record
     *****************************************************/
    getRecordCount: function(storeName){
        var value = "";
        try {
            /** 
             * Count number of records in the store
             */
            value = storeName.getCount();
            if (isLogEnabled) {
                console.log("LOG: Get Record Count successfully.");
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. Get Record count failed.");
                console.log(err.description);
            }
        }
        return value;
    }, /* end of getRecordCount */
    /******************************************************
     * Update the record
     * @param {Html5 localstorage} storeName The name of Html5 database object
     * @param {Record object} record The record to be updated
     * @param {String} fieldName Name of the field
     * @param {String} fieldValue Value of the field
     *****************************************************/
    updateRecord: function(storeName, record, fieldName, fieldValue){
        try {
            record.set(fieldName, fieldValue);
            storeName.sync();
            
            if (isLogEnabled) {
                console.log("LOG: Update Record successfully.");
            }
        } 
        catch (err) {
            if (isLogEnabled) {
                console.log("LOG: Error. Update Record failed.");
                console.log(err.description);
            }
        }
    } /* end of updateRecord */
};


//
//Sample Usage
//
//  var storeName = "myDatabase";
//  var storeFields = ['id','name'];
//  SP.Database.createStore(storeName, storeFields);
