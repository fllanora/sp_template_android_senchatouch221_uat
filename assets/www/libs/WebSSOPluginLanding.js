/*****************************************************
 * WebSSO Library
 *
 * LANDING PAGE
 *
 *****************************************************/
function WebSSOlandingPage(){
    if (isLogEnabled) {
        console.log("LOG: WebSSOlandingPage()");
    }

	Ext.Viewport.removeAll(true,true);
	Ext.Viewport.add(Ext.create('app.view.mainView'));
}
