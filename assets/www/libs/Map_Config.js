/******************************************************
 * Map Library Configuration File
 *****************************************************/
var MAP_SERVER01 = "http://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer";
var ERROR_LOCATION_PERMISSION_DENIED = "Location not provided";
var ERROR_LOCATION_POSITION_UNAVAILABLE = "Current location not available";
var ERROR_LOCATION_TIMEOUT = "Timeout";
var ERROR_LOCATION_OTHER = "Unknown Error";
var LOCATION_MARKER_GRAPHIC = "images/greendot.png";
var LOCATION_MARKER_GRAPHIC_HEIGHT = 40;
var LOCATION_MARKER_GRAPHIC_WIDTH = 40;
var LOCATION_ZOOM_LEVEL = 17;
var DIV_MAP_NAME = "map";
