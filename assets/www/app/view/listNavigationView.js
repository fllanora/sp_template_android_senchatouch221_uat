Ext.define('app.view.listNavigationView', {
    extend: 'Ext.NavigationView',
    xtype: 'listNavigationView',
    
   requires: [
       'app.view.myList',
       'app.view.myListDetails'
   ],

    config: {
        title: 'List',
        iconCls: 'user',
        navigationBar: {
            items: [{
                xtype: 'button',
                align: 'right',
                iconCls: 'home',
                iconMask: true,
                hidden: false,
                action: 'logout'
            }, {
                xtype: 'button',
                align: 'right',
                iconCls: 'action',
                iconMask: true,
                hidden: true,
                action: 'action'
            }]
        },

       items:[{
            title: 'List',
            xtype: 'myList'
        }]

    }

});


