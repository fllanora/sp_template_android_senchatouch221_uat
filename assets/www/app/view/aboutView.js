Ext.define('app.view.aboutView', {
    extend: 'Ext.Panel',
    xtype: 'aboutView',
    
   requires: [
       'Ext.TitleBar'
   ],
    
    config: {
        title: 'About',
        iconCls: 'more',
        scrollable: 'vertical',
        styleHtmlContent: true,

        items:[{
            xtype: 'titlebar',
            title: 'About',
            docked: 'top'
        }, {
            html: [
                '<p>Built with Sencha Touch V' + Ext.getVersion('touch').toString() + '</p>'
            ].join('')
        }]

    }
});