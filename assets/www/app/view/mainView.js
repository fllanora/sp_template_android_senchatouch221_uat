Ext.define('app.view.mainView', {
    extend: 'Ext.tab.Panel',
    xtype: 'mainView',

    requires: [
       'app.view.listNavigationView',
       'app.view.aboutView'
   ],
    config: {
        tabBar: {
            docked: 'bottom'
        },
        items: [{
            xtype: 'listNavigationView'
        },{
            xtype: 'aboutView'
        }]

    }
});
