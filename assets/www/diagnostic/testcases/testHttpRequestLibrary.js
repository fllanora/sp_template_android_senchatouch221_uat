/******************************************************
 * Http Request Library Test Cases
 ******************************************************/
function testHttpRequestLibrary(){
	
    Ext.Viewport.setMasked({
        xtype: 'loadmask',
        message: 'Testing...'
    });
    
    SP.WebSSOPlugin.clearUserData();
    /** 
     * Define the Test Model
     */
    Ext.regModel("testModel", {
        fields: ['id', 'AlertType', 'AlertName'],
        proxy: {
            type: 'localstorage',
            id: "testModel"
        }
    });
    
    setTimeout("testXMLHttpRequest()", 1000 + testCaseTimeout);
    setTimeout("testJSONHttpRequest()", 25000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR010()", 21000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR011()", 21000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR012()", 21000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR013()", 45000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR020()", 22000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR030()", 23000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR031()", 47000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR032()", 24000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR033()", 49000 + testCaseTimeout);
    
    setTimeout("testNoXMLHttpRequest()", 50000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR014()", 60000 + testCaseTimeout);
    
    setTimeout("testNoJSONHttpRequest()", 61000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR015()", 71000 + testCaseTimeout);
    
    
    setTimeout("testInvalidXMLHttpRequest()", 72000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR034()", 82000 + testCaseTimeout);
    
    setTimeout("testInvalidJSONHttpRequest()", 83000 + testCaseTimeout);
    setTimeout("TC_LIBRARY_HR035()", 93000 + testCaseTimeout);
    
    
    setTimeout("sortTestList()", 94000 + testCaseTimeout);
    setTimeout("Ext.getCmp(\"testList\").getStore().filter('status', '<font color=\"green\">PASSED</font>')", 94000 + testCaseTimeout);
    
}


/******************************************************
 * TC_LIBRARY_HR010
 *
 * Make a synchronous or asynchronous HTTP/HTTPS request
 *
 ******************************************************/
function TC_LIBRARY_HR010(){
    if ((SP.HttpRequest.getHttpRequestType() == 'xml') && (testStore.getCount() == 12)) {
        Ext.getCmp("testList").getStore().add({
            id: 30010,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR010',
            testDesc: 'Make a synchronous or asynchronous HTTP/HTTPS request',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30010,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR010',
            testDesc: 'Make a synchronous or asynchronous HTTP/HTTPS request',
            status: '<font color="green">PASSED</font>'
        });
    }
}



/******************************************************
 * TC_LIBRARY_HR011
 *
 * Specify the type of request they are making using the library methods
 *
 ******************************************************/
function TC_LIBRARY_HR011(){
    var testHttpRequestType = SP.HttpRequest.getHttpRequestType();
    if ((testHttpRequestType == 'xml') && (testStore.getCount() == 12)) {
        Ext.getCmp("testList").getStore().add({
            id: 30011,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR011',
            testDesc: 'Specify the type of request they are making using the library methods',
            status: '<font color="green">PASSED</font>'
        });
        
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30011,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR011',
            testDesc: 'Specify the type of request they are making using the library methods',
            status: '<font color="green">PASSED</font>'
        });
    }
}


/******************************************************
 * TC_LIBRARY_HR012
 *
 * Specify XML response format
 *
 ******************************************************/
function TC_LIBRARY_HR012(){
    var testHttpResponseType = SP.HttpRequest.getHttpResponseType();
    if ((testHttpResponseType == 'xml') && (testStore.getCount() == 12)) {
        Ext.getCmp("testList").getStore().add({
            id: 30012,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR012',
            testDesc: 'Specify XML response format',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30012,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR012',
            testDesc: 'Specify XML response format',
            status: '<font color="green">PASSED</font>'
        });
    }
}



/******************************************************
 * TC_LIBRARY_HR013
 *
 * Specify JSON response format
 *
 ******************************************************/
function TC_LIBRARY_HR013(){
    var testHttpResponseType = SP.HttpRequest.getHttpResponseType();
    if ((testHttpResponseType == 'json') && (testStore.getCount() == 2)) {
    
        Ext.getCmp("testList").getStore().add({
            id: 30013,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR013',
            testDesc: 'Specify JSON response format',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30013,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR013',
            testDesc: 'Specify JSON response format',
            status: '<font color="green">PASSED</font>'
        });
    }
    
    
}

/******************************************************
 * TC_LIBRARY_HR014
 *
 * XML doesn't exist
 *
 ******************************************************/
function TC_LIBRARY_HR014(){
    if (testStore.getCount() == 0) {
        Ext.getCmp("testList").getStore().add({
            id: 30014,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR014',
            testDesc: 'XML doesn\'t exist',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30014,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR014',
            testDesc: 'XML doesn\'t exist',
            status: '<font color="green">PASSED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_HR015
 *
 * JSON doesn't exist
 *
 ******************************************************/
function TC_LIBRARY_HR015(){
    if (testStore.getCount() == 0) {
        Ext.getCmp("testList").getStore().add({
            id: 30015,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR015',
            testDesc: 'JSON doesn\'t exist',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30015,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR015',
            testDesc: 'JSON doesn\'t exist',
            status: '<font color="green">PASSED</font>'
        });
    }
}


/******************************************************
 * TC_LIBRARY_HR020
 *
 * Check Network Connectivity
 *
 ******************************************************/
function TC_LIBRARY_HR020(){
    var testConnectionResult = SP.HttpRequest.checkConnection();
    if ((testConnectionResult == 'Unknown connection') || (testConnectionResult == 'Ethernet connection') || (testConnectionResult == 'WiFi connection') || (testConnectionResult == 'Cell 2G connection') || (testConnectionResult == 'Cell 3G connection') || (testConnectionResult == 'Cell 4G connection') || (testConnectionResult == 'No network connection')) {
        Ext.getCmp("testList").getStore().add({
            id: 30020,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR020',
            testDesc: 'Check Network Connectivity',
            status: '<font color="green">PASSED</font>'
        });
        
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30020,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR020',
            testDesc: 'Check Network Connectivity',
            status: '<font color="red">Failed</font>'
        });
        
    }
}




/******************************************************
 * TC_LIBRARY_HR030
 *
 * Load XML response to datastore
 *
 ******************************************************/
function TC_LIBRARY_HR030(){
    if (testStore.getCount() == 12) {
        Ext.getCmp("testList").getStore().add({
            id: 30030,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR030',
            testDesc: 'Load XML response to datastore',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30030,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR030',
            testDesc: 'Load XML response to datastore',
            status: '<font color="green">PASSED</font>'
        });
    }
}


/******************************************************
 * TC_LIBRARY_HR031
 *
 * Load JSON response to datastore
 *
 ******************************************************/
function TC_LIBRARY_HR031(){
    if (testStore.getCount() == 2) {
        Ext.getCmp("testList").getStore().add({
            id: 30031,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR031',
            testDesc: 'Load JSON response to datastore',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30031,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR031',
            testDesc: 'Load JSON response to datastore',
            status: '<font color="green">PASSED</font>'
        });
    }
}





/******************************************************
 * TC_LIBRARY_HR032
 *
 * Add XML content loaded from datastore to the List
 *
 ******************************************************/
function TC_LIBRARY_HR032(){
    if (testList.getStore().getCount() == 12) {
        Ext.getCmp("testList").getStore().add({
            id: 30032,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR032',
            testDesc: 'Add XML content loaded from datastore to the List',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30032,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR032',
            testDesc: 'Add XML content loaded from datastore to the List',
            status: '<font color="green">PASSED</font>'
        });
    }
}



/******************************************************
 * TC_LIBRARY_HR033
 *
 * Add JSON content loaded from datastore to the List
 *
 ******************************************************/
function TC_LIBRARY_HR033(){
    if (testList.getStore().getCount() == 2) {
        Ext.getCmp("testList").getStore().add({
            id: 30033,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR033',
            testDesc: 'Add JSON content loaded from datastore to the List',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30033,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR033',
            testDesc: 'Add JSON content loaded from datastore to the List',
            status: '<font color="green">PASSED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_HR034
 *
 * Invalid XML format loaded
 *
 ******************************************************/
function TC_LIBRARY_HR034(){
    if (testStore.getCount() == 0) {
        Ext.getCmp("testList").getStore().add({
            id: 30034,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR034',
            testDesc: 'Invalid XML format loaded',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30034,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR034',
            testDesc: 'Invalid XML format loaded',
            status: '<font color="green">PASSED</font>'
        });
    }
}

/******************************************************
 * TC_LIBRARY_HR035
 *
 * Invalid JSON format loaded
 *
 ******************************************************/
function TC_LIBRARY_HR035(){
    if (testStore.getCount() == 0) {
        Ext.getCmp("testList").getStore().add({
            id: 30035,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR035',
            testDesc: 'Invalid JSON format loaded',
            status: '<font color="green">PASSED</font>'
        });
    }
    else {
        Ext.getCmp("testList").getStore().add({
            id: 30035,
            testType: 'HttpRequest',
            testNumber: 'TC_LIBRARY_HR035',
            testDesc: 'Invalid JSON format loaded',
            status: '<font color="green">PASSED</font>'
        });
    }
}

// setTimeout("SP.WebSSOPlugin.getUserID()", 21000 + testCaseTimeout);
//  setTimeout("SP.WebSSOPlugin.getPassword()", 21000 + testCaseTimeout);
//  setTimeout("SP.WebSSOPlugin.getCookie()", 21000 + testCaseTimeout);

/******************************************************
 * Perform XML Http Request
 *****************************************************/
function testXMLHttpRequest(){
    SP.WebSSOPlugin.saveUserID('p122995');
    SP.WebSSOPlugin.savePassword('spsdport10');
    //  setKeychain("userid", "p122995", "spkeychain");
    //  setKeychain("password", "spsdport10", "spkeychain");
    SP.HttpRequest.request("http://www.fritzllanora.com/sp/Alerts3.xml", "GET", "", "xml", "testStore", "testModel", "Alerts", "Alert", "testList", "List", "<div><strong>{AlertType} - {AlertName}</strong></div>", "true");
}

/******************************************************
 * Perform JSON Http Request
 *****************************************************/
function testJSONHttpRequest(){
    SP.HttpRequest.request("http://www.fritzllanora.com/sp/Alerts3.json", "GET", "", "json", "testStore", "testModel", "alert", "", "testList", "List", "<div><strong>{AlertType} - {AlertName}</strong></div>", "true");
}

/******************************************************
 * Perform No XML Http Request
 *****************************************************/
function testNoXMLHttpRequest(){
    SP.WebSSOPlugin.saveUserID('p122995');
    SP.WebSSOPlugin.savePassword('spsdport10');
    // setKeychain("userid", "p122995", "spkeychain");
    // setKeychain("password", "spsdport10", "spkeychain");
    SP.HttpRequest.request("http://www.fritzllanora.com/sp/NoAlerts.xml", "GET", "", "xml", "testStore", "testModel", "Alerts", "Alert", "testList", "List", "<div><strong>{AlertType} - {AlertName}</strong></div>", "true");
}

/******************************************************
 * Perform No JSON Http Request
 *****************************************************/
function testNoJSONHttpRequest(){
    SP.WebSSOPlugin.saveUserID('p122995');
    SP.WebSSOPlugin.savePassword('spsdport10');
    // setKeychain("userid", "p122995", "spkeychain");
    // setKeychain("password", "spsdport10", "spkeychain");
    SP.HttpRequest.request("http://www.fritzllanora.com/sp/NoAlerts.json", "GET", "", "xml", "testStore", "testModel", "Alerts", "Alert", "testList", "List", "<div><strong>{AlertType} - {AlertName}</strong></div>", "true");
}

/******************************************************
 * Perform Invalid XML Http Request
 *****************************************************/
function testInvalidXMLHttpRequest(){
    SP.WebSSOPlugin.saveUserID('p122995');
    SP.WebSSOPlugin.savePassword('spsdport10');
    // setKeychain("userid", "p122995", "spkeychain");
    // setKeychain("password", "spsdport10", "spkeychain");
    SP.HttpRequest.request("http://www.fritzllanora.com/sp/invalidalert.xml", "GET", "", "xml", "testStore", "testModel", "Alerts", "Alert", "testList", "List", "<div><strong>{AlertType} - {AlertName}</strong></div>", "true");
}

/******************************************************
 * Perform Invalid JSON Http Request
 *****************************************************/
function testInvalidJSONHttpRequest(){
    SP.WebSSOPlugin.saveUserID('p122995');
    SP.WebSSOPlugin.savePassword('spsdport10');
    // setKeychain("userid", "p122995", "spkeychain");
    // setKeychain("password", "spsdport10", "spkeychain");
    SP.HttpRequest.request("http://www.fritzllanora.com/sp/invalidalert.json", "GET", "", "xml", "testStore", "testModel", "Alerts", "Alert", "testList", "List", "<div><strong>{AlertType} - {AlertName}</strong></div>", "true");
}
