package com.example.webssotest;



import org.apache.cordova.DroidGap;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

public class MainActivity extends DroidGap {
	
	private static Context context;
	public static String messageID = "";
	public static String messageData = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); 	
        super.setIntegerProperty("splashscreen", R.drawable.splash);
		MainActivity.context = getApplicationContext();
        /*****************************************************
         * For SP Template 
         *****************************************************/
	//	super.loadUrl("file:///android_asset/www/index.html");
        /*****************************************************
         * For SP Library Test Suite (Diagnostic)
         *****************************************************/
		super.loadUrl("file:///android_asset/www/diagnostic.html");
    }
    
    /*****************************************************
     * Get Main Activity Context
     *****************************************************/
    public static Context getAppContext() {
        return MainActivity.context;
     }
}
