package com.example.webssotest;

import java.io.IOException;
import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

import com.google.*;
import com.google.android.*;
import com.google.android.gcm.*;

public class GCMIntentService extends GCMBaseIntentService {

    private static final String LOG_TAG = "GetAClue::GCMIntentService";
	private static String senderID = "295234647339";
	private static int alertNumber = 0;
	private static String registrationToken = null;
	private static Random rdm;
	public static String messageAlert = "";
	public static String messageID = "";
	public static final String NOTIFICATION_DATA = "NOTIFICATION_DATA";
	public static final String NOTIFICATION_ID = "NOTIFICATION_ID";

    public GCMIntentService() {
        super(senderID);
        // TODO Auto-generated constructor stub
        Log.i( LOG_TAG, "GCMIntentService constructor called" );
    	rdm = new Random();
    }

    
	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(Context context, String message, String messageID) {
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.ic_launcher,
				message, when);
		String title = context.getString(R.string.app_name);
		Intent notificationIntent = new Intent(context,
				MainActivity.class);
		// set intent so it does not start a new activity
	//	FLAG_CANCEL_CURRENT
		//notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
		//		| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
	
	
		notificationIntent.putExtra(NOTIFICATION_DATA, message);
		notificationIntent.putExtra(NOTIFICATION_ID, messageID);

		/*create unique this intent from  other intent using setData */
		notificationIntent.setData(Uri.parse("content://"+when));
		
		PendingIntent intent = PendingIntent.getActivity(context, Integer.parseInt(messageID), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
	//	 Intent intent = new Intent(main.getAppContext(),main.class); 
		//notificationIntent.putExtra("message", message);
		//notificationIntent.putExtra("id", "100");
		// PendingIntent contentIntent = PendingIntent.getActivity(this, 1, intent, 0);
		 
				
		
		//PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		//intent.
	     // messageID = context. //intent .getStringExtra( "id" );
	    // messageAlert =  //intent.getStringExtra( "message" ) 
		
	//	PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
	//	Bundle extras = notificationIntent.getExtras(); //getIntent().getExtras();

		
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		alertNumber = rdm.nextInt(10000);
		notificationManager.notify(alertNumber, notification);

		
	}

    public static String getRegistrationToken() {
        return registrationToken;
    }
	
    @Override
    protected void onError( Context arg0, String errorId ) {
        // TODO Auto-generated method stub
        Log.i( LOG_TAG, "GCMIntentService onError called: " + errorId );
    }

    @Override
    protected void onMessage( Context arg0, Intent intent ) {
        // TODO Auto-generated method stub
        Log.v( LOG_TAG, "GCMIntentService onMessage called" );
        Log.v( LOG_TAG, "Message is: " + intent.getStringExtra( "message" ) );
        Log.v( LOG_TAG, "ID is: " + intent.getStringExtra( "id" ));
     //   messageID = intent.getStringExtra( "id" );
     //   messageAlert =  intent.getStringExtra( "message" ) ;
        generateNotification(arg0, intent.getStringExtra("message"), intent.getStringExtra("id"));
    }

    @Override
    protected void onRegistered( Context arg0, String registrationId ) {
        // TODO Auto-generated method stub
        Log.v( LOG_TAG, ">>>>>>>>>>>>>> GCMIntentService onRegistered called" );
        Log.v( LOG_TAG, "Registration id is: " + registrationId );
        registrationToken = registrationId; 
    }

    @Override
    protected void onUnregistered( Context arg0, String registrationId ) {
        // TODO Auto-generated method stub
        Log.i( LOG_TAG, "GCMIntentService onUnregistered called" );
        Log.i( LOG_TAG, "Registration id is: " + registrationId );
    }
    
}