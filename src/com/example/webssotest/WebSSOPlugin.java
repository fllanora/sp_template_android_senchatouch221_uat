package com.example.webssotest;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Environment;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;

public class WebSSOPlugin extends CordovaPlugin {

	private static final String TAG = null;
	private static String responseHeaderCallback = null;
	private static String responseCallback = null;
	private static String postType = null;
	private static String useridRead = null;
	private static String passwordRead = null;
	private static String cookieRead = null;
	private static String userID = null;
	private static String userPassword = null;
	private static String WebSSODomain = "";
	private FileWriter writer;
	private FileReader reader;
	private File fileSave;

	public static Cookie cookie = null;
	
	 @Override
	    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException 
	    {

	//@Override
	//public PluginResult execute(String action, JSONArray data, String callbackId) {

		PluginResult result = null;
		String baseDir = Environment.getExternalStorageDirectory()
				.getAbsolutePath();
		try {

			/*****************************************************
			 * Get User Data Stored in Account Authenticator
			 *****************************************************/
			if (action.equals("getUserData")) {
				Log.v(TAG, "LOG: getUserData ");
				
				String keyvalue = "";
				try {
					String key = data.getString(0);
					/*****************************************************
					 * Create Account for AccountManager
					 *****************************************************/
					Account account = new Account("SP_Account",
							 this.cordova.getActivity().getPackageName());
					/*****************************************************
					 * Define AccountManager
					 *****************************************************/
					AccountManager accountManager = AccountManager
							.get(this.cordova.getActivity());
					keyvalue = accountManager.getUserData(account, key);
					Log.v(TAG, "LOG: keyvalue: " + keyvalue);
				} catch (Exception e) {
					Log.v(TAG, "LOG: ERROR");
					e.printStackTrace();
				}
				  this.echo(keyvalue, callbackContext);
				return true;
			}

			/*****************************************************
			 * Set the WebSSO Domain
			 *****************************************************/
			if (action.equals("setWebSSODomain")) {
				WebSSODomain = data.getString(0);
				Log.v(TAG, "LOG: Set WebSSODomain:" + WebSSODomain);
			}

			/*****************************************************
			 * Save User Data to Account Authenticator
			 *****************************************************/
			if (action.equals("saveUserData")) {
				Log.v(TAG, "LOG: saveUserData");

				try {
					String key = data.getString(0);
					String value = data.getString(1);
					/*****************************************************
					 * Create Account for AccountManager
					 *****************************************************/
					Account account = new Account("SP_Account",
							this.cordova.getActivity().getPackageName());
					/*****************************************************
					 * Define AccountManager
					 *****************************************************/
					AccountManager accountManager = AccountManager
							.get(this.cordova.getActivity());
					accountManager.addAccountExplicitly(account, "", null);
					accountManager.setUserData(account, key, value);
					String keyvalue = accountManager.getUserData(account, key);
					Log.v(TAG, "LOG: keyvalue: " + keyvalue);
				} catch (Exception e) {
					Log.v(TAG, "LOG: ERROR");
					e.printStackTrace();
				}
		      this.echo("success", callbackContext);
		      return true;
			}

			/*****************************************************
			 * Perform WebSSO forpost Authentication
			 *****************************************************/
			if (action.equals("formPost")) {

				userID = data.getString(1);
				userPassword = data.getString(2);
				postType = data.getString(4);
				Log.v(TAG, "LOG: formpost");

				/*****************************************************
				 * Enable Ajax calls to access Phonegap Webview cookies
				 *****************************************************/
				// try{
				// this.webView.getSettings().setJavaScriptEnabled(true);
				// }
				// catch(Exception e){}

				/*****************************************************
				 * Define Cookiemanager for managing cookies
				 *****************************************************/
				CookieSyncManager.createInstance(this.cordova.getActivity());
				CookieManager cookieManager = CookieManager.getInstance();
				cookieManager.removeSessionCookie();
				CookieSyncManager.getInstance().sync();

				CookieStore cookieStore;
				HttpContext httpContext;
				HttpGet httpGet;
				HttpResponse httpResponse = null;
				HttpPost post;
				DefaultHttpClient httpClient;
				/*****************************************************
				 * Define Basic Cookie Store
				 *****************************************************/
				cookieStore = new BasicCookieStore();
				httpContext = new BasicHttpContext();
				httpContext.setAttribute(ClientContext.COOKIE_STORE,
						cookieStore);
				httpClient = (DefaultHttpClient) new MySSLSocketFactory(null)
						.getNewHttpClient();
				/*****************************************************
				 * Create Headers List
				 *****************************************************/
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("username", userID));
				params.add(new BasicNameValuePair("password", userPassword));
				params.add(new BasicNameValuePair("login-form-type", "pwd"));

				/*****************************************************
				 * Check the WebSSO Environment server type
				 *****************************************************/
				Log.v(TAG, "LOG: Test server");
				httpGet = new HttpGet("https://" + WebSSODomain
						+ "/pkmslogin.form");
				post = new HttpPost("https://" + WebSSODomain
						+ "/pkmslogin.form");
				/*****************************************************
				 * Handle the Http Redirect Callback (301/302)
				 *****************************************************/
				httpClient.setRedirectHandler(new DefaultRedirectHandler() {
					@Override
					public boolean isRedirectRequested(HttpResponse response,
							HttpContext context) {
						boolean isRedirect = super.isRedirectRequested(
								response, context);
						int responseCode = response.getStatusLine()
								.getStatusCode();
						if (isRedirect) {
							/*****************************************************
							 * Check Response code
							 *****************************************************/
							Log.v(TAG, "LOG: Process Response Code");
							if (responseCode == 301 || responseCode == 302) {
								if (postType.equals("credentialsLogin")) {
									try {
										/*****************************************************
										 * Gets all the cookie in the response
										 * Header "Set-Cookie" and store in a
										 * variable
										 *****************************************************/
										Header responseHeader = response
												.getLastHeader("Set-Cookie");
										responseHeaderCallback = responseHeader
												.getValue() + ";";
									} catch (Exception e) {
										Log.v(TAG, "LOG: Error");
										responseHeaderCallback = "";
										e.printStackTrace();
									}
								}
								if (postType.equals("cookieLogin")) {
									try {
										/*****************************************************
										 * Gets the Response Http Header
										 * "Location"
										 *****************************************************/
										Header responseHeader = response
												.getLastHeader("Location");
										responseHeaderCallback = responseHeader
												.getValue() + ";";
									} catch (Exception e) {
										responseHeaderCallback = "";
										Log.v(TAG, "LOG: Error");
										e.printStackTrace();
									}
								}
								return true;
							}
						}
						return isRedirect;
					}
				});

				String tempResponse = "";
				try {
					if (postType.equals("credentialsLogin")) {
						/*****************************************************
						 * WebSSO Authentication using credentials
						 *****************************************************/
						
						StringBuilder sb = new StringBuilder();
						Iterator<NameValuePair> i = params.iterator();
						while( i.hasNext() )
						{
						    sb.append( i.next().toString() );
						    if( i.hasNext() )
						        sb.append("&");
						}
						HttpEntity entity = new StringEntity(sb.toString());
						
						//post.setEntity(new UrlEncodedFormEntity(params));
						post.setEntity(entity);
						httpResponse = httpClient.execute(post, httpContext);
					}
					if (postType.equals("cookieLogin")) {
						/*****************************************************
						 * WebSSO Authentication using session cookies
						 *****************************************************/
						String fileHeaderCookie = data.getString(3);
						httpGet.addHeader("Cookie", fileHeaderCookie);
						httpResponse = httpClient.execute(httpGet, httpContext);
					}
					int responseCode = httpResponse.getStatusLine()
							.getStatusCode();
					/*****************************************************
					 * Handle the Http Response 200
					 ***************************** ************************/
					if (responseCode == 200) {
						Log.v(TAG, "LOG: Response Code 200");
						if (postType.equals("credentialsLogin")) {
							try {

								/*****************************************************
								 * Gets all the cookie in the response Header
								 * "Set-Cookie" and store in a variable
								 *****************************************************/
								for (Header h : httpResponse.getAllHeaders()) {
									String headerName = h.getName() + " = "
											+ h.getValue();
									if (h.getName().equals("Set-Cookie")) {
										tempResponse = tempResponse
												+ h.getValue() + ";";
									}
								}
							} catch (Exception e) {
								tempResponse = "";
								e.printStackTrace();
							}
						}
						/*****************************************************
						 * Combines the session cookies in the redirect response
						 * and final response
						 *****************************************************/
						responseHeaderCallback = responseHeaderCallback
								+ tempResponse;
					}
				} catch (ClientProtocolException e) {
					Log.v(TAG, "LOG: ClientProtocolException Error");
					e.printStackTrace();
				} catch (IOException e) {
					Log.v(TAG, "LOG: IOException Error");
					e.printStackTrace();
				}

				/*****************************************************
				 * Process Session Cookies
				 *****************************************************/
				Cookie sessionCookie = null;
				if (postType.equals("credentialsLogin")) {
					try {
						List<Cookie> cookies = cookieStore.getCookies();
						Log.v(TAG, "LOG: cookie size" + cookies.size());
						for (int i = 0; i < cookies.size(); i++) {
							cookie = cookies.get(i);
							Log.v(TAG,
									"LOG: cookie " + i + " = " + cookies.get(i));
							sessionCookie = cookies.get(i);
							if (sessionCookie != null) {
								String cookieString = sessionCookie.getName()
										+ "=" + sessionCookie.getValue()
										+ "; domain="
										+ sessionCookie.getDomain();
								Log.v(TAG, "LOG: cookieString " + cookieString);
								CookieManager.getInstance().setCookie(
										WebSSODomain, cookieString);
								/*****************************************************
								 * Sync the CookieSyncManager to save the
								 * cookies
								 *****************************************************/
								CookieSyncManager.getInstance().sync();
							}

						}
					} catch (Exception e) {
						Log.v(TAG, "LOG: LOGIN cookie error");
						e.printStackTrace();
					}
				} else if (postType.equals("cookieLogin")) {
					String fileHeaderCookie = data.getString(3);
					String[] items = fileHeaderCookie.split(";");
					for (int i = 0; i < items.length; i++) {
						try {
							String cookieString = items[i] +"; domain="
									+ WebSSODomain;
							Log.v(TAG, "cookieString " + cookieString);
							CookieManager.getInstance().setCookie(
									WebSSODomain, cookieString);
							CookieSyncManager.getInstance().sync();
							
							cookieString = items[i] +"; domain=sso.sp.edu.sg";
							CookieManager.getInstance().setCookie(
									"sso.sp.edu.sg", cookieString);
							CookieSyncManager.getInstance().sync();
							
							cookieString = items[i] +"; domain=sso-tst.testsf.testsp.edu.sg";
							CookieManager.getInstance().setCookie(
									"sso-tst.testsf.testsp.edu.sg", cookieString);
							CookieSyncManager.getInstance().sync();
							
							cookieString = items[i] +"; domain=eliser.lib.sp.edu.sg";
							CookieManager.getInstance().setCookie(
									"eliser.lib.sp.edu.sg", cookieString);
							CookieSyncManager.getInstance().sync();
							
							cookieString = items[i] +"; domain=m.lib.sp.edu.sg";
							CookieManager.getInstance().setCookie(
									"m.lib.sp.edu.sg", cookieString);
							CookieSyncManager.getInstance().sync();
							
							cookieString = items[i] +"; domain=esp.sp.edu.sg";
							CookieManager.getInstance().setCookie(
									"esp.sp.edu.sg", cookieString);
							CookieSyncManager.getInstance().sync();
						} catch (Exception e) {
						}

					}
				}
			      this.echo(responseHeaderCallback, callbackContext);
			      return true;
			
			} else {
				  this.echo("", callbackContext);
			      return true;
			}

		} catch (Exception e) {
			//result = new PluginResult(Status.ERROR);
			  this.echo("", callbackContext);
		     // return true;
			Log.v(TAG, "LOG: Error");
			e.printStackTrace();
		}
		return false;
	}
	 
	  private void echo(String message, CallbackContext callbackContext) {
	      //  if (message != null && message.length() > 0) {
	            callbackContext.success(message);
	     //   } else {
	       //     callbackContext.error("Expected one non-empty string argument.");
	    //    }
	    }

}